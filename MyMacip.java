import java.net.*; 
 
public class My_mac_ip
{
    public static void main(String argv[]) throws Exception 
	{
        InetAddress host = InetAddress.getLocalHost();
        byte ip[] = host.getAddress();
        System.out.println(host.getHostName());
        for (int i = 0; i < ip.length; i++) 
		{
            if (i > 0) System.out.print(".");
            System.out.print(ip[i] & 0xff);
        }
        System.out.println();
        Process p = Runtime.getRuntime().exec("getmac /fo csv /nh");
        java.io.BufferedReader in = new java.io.BufferedReader(new java.io.InputStreamReader(p.getInputStream()));
        String line = in.readLine();
        String[] result = line.split(",");
        System.out.println(result[0].replace('"', ' ').trim());
    }
}