import java.math.BigInteger;
import java.util.Scanner;

public class Forbig
{
    public static BigInteger big(BigInteger a, BigInteger b) 
	{

        if (b.equals(BigInteger.valueOf(0)))
		{
            return (a);
        } else 
		{
            a = a.remainder(b);
            return big(b, a);
        }
    }

    public static void main(String[] args) 
	{
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        BigInteger e = BigInteger.valueOf(2);
        BigInteger x = (e.pow(a).subtract(BigInteger.valueOf(1)));
        BigInteger y = (e.pow(b).subtract(BigInteger.valueOf(1)));
        System.out.println(big(x, y));
    }
}