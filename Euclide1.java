import java.util.Scanner;

public class Euclide 
{

    public static void main(String args[]) throws Exception 
	{
        Scanner in = new Scanner(System.in);
        long a = in.nextLong();
        long b = in.nextLong();
        while (true)
		{
            if (a == 0 || b == 0) 
			{
                System.out.println(a);
                break;
            }
            if (b >= a) 
			{
                b = b - a;
            } else 
			{
                a = a - b;
            }
        }
    }
}