import java.util.Scanner;

public class Finfxy 
{

    public static long[] X(long[] m) throws Exception 
	{
        long r;
        long a = m[1];
        long b = m[2];
        if (b >= a) 
		{
            r = a;
            a = b;
            b = r;
        }
        int i = 3;
        while (true) 
		{
            if (a % b == 0)
			{
                m[0] = b;
                return m;
            }
            r = b;
            m[i] = a / b;
            b = a % b;
            i++;
            a = r;
        }
    }

    public static void main(String args[]) throws Exception 
	{
        System.out.println("a*x+b*y=1" + (char) 10 + "Input something a and b");
        Scanner in = new Scanner(System.in);
        long a = in.nextLong();
        long b = in.nextLong();
        long[] m = new long[(int) Math.cbrt(Math.max(a,b)) + 4];
        m[1] = a;
        m[2] = b;
        m = X(m);
        if (!(m[0] == 1)) 
		{
            System.out.println("No solution");
            return;
        }
        a = Math.max(a, b);
        long mt[][] = new long[3][(int) Math.cbrt(a) + 3];
        mt[1][2] = 1;
        mt[1][1] = 0;
        mt[2][2] = 0;
        mt[2][1] = 1;
        int i;
        for (i = 3; i <= (int) Math.cbrt(a) + 3; i++) 
		{
            if (m[i] == 0) 
			{
                break;
            }
            mt[0][i] = m[i];
            mt[1][i] = m[i] * mt[1][i - 1] + mt[1][i - 2];
            mt[2][i] = m[i] * mt[2][i - 1] + mt[2][i - 2];
        }
        a = m[1];
        if ((mt[2][i - 1] * a) - (mt[1][i - 1] * b) == 1) 
		{
            System.out.println("x=" + mt[2][i - 1] + ";" + (char) 10 + "y=" + -mt[1][i - 1] + ";");
        } else if ((mt[1][i - 1] * b) - (mt[2][i - 1] * a) == 1) 
		{
            System.out.println("x=" + -mt[2][i - 1] + ";" + (char) 10 + "y=" + mt[1][i - 1] + ";");
        } else if ((mt[1][i - 1] * a) - (mt[2][i - 1] * b) == 1) 
		{
            System.out.println("x=" + mt[1][i - 1] + ";" + (char) 10 + "y=" + -mt[2][i - 1] + ";");
        } else if ((mt[2][i - 1] * b) - (mt[1][i - 1] * a) == 1) 
		{
            System.out.println("x=" + -mt[1][i - 1] + ";" + (char) 10 + "y=" + mt[2][i - 1] + ";");
        } else {
            System.out.println("Something wrong");
        }
    }
}