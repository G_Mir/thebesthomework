import java.util.Arrays;
import java.io.File;
import java.io.FileWriter;


public class Task032WithoutNewFile 
{


    public static String[] l;

    public static void stat(Tree t, String s) 
	{
        if (t.left == null) 
		{
            l[(int) t.id] = s;
        } else {
            stat(t.left, s + "0");
            stat(t.right, s + "1");
        }
    }

    public static void main(String args[]) throws Exception 
	{
        int[] m = Task031.main(new String[0]);
        l = new String[300];
        FileWriter file = new FileWriter(new File("Answer.txt"), true);
        int k1 = 0;
        for (int i = 0; i <= 255; i++)
		{
            if (!(m[i] == 0)) 
			{
                k1 = i;
                break;
            }
        }
        Tree t = new Tree((char) k1);
        for (int i = k1 + 1; i <= 255; i++) 
		{
            if (!(m[i] == 0)) 
			{
                t = new Tree(new Tree((char) i), t);
            }
        }
        Arrays.fill(l, "");
        stat(t, "");
        for (int i = 0; i < l.length; i++) 
		{
            if (!(l[i].equals(""))) 
			{
                file.write(((char) i) + " - " + l[i] + (char) 10);
            }
        }
        file.close();
    }
}