import java.io.*;
import java.util.Scanner;


public class Decoder 
{

    public static int st(String[] l, String str) 
	{
        for (int i = 0; i <= 255; i++) 
		{
            if (l[i].length() >= 1) 
			{
                boolean t = str.startsWith(l[i]);
                if (t) 
				{
                    return i;
                }
            }
        }
        return -1;
    }

    public static void main(String args[]) throws Exception 
	{
        Scanner in = new Scanner(System.in);
        try 
		{
            System.out.println("Input your encrypted file");
            String str = in.nextLine();
            FileInputStream fis = new FileInputStream(str);
            try 
			{
                System.out.println("Input your key file");
                str = in.nextLine();
                Scanner fin = new Scanner(new File(str));
                System.out.println("Input name for you decrypted file");
                str = in.nextLine();
                File f1 = new File(str);
                RandomAccessFile file = new RandomAccessFile(f1, "rw");
                String[] l = new String[256];
                for (int i = 0; i <= 255; i++)
				{
                    l[i] = fin.nextLine();
                }
                int i5 = fin.nextInt();
                int i6 = i5;
                str = "";
                try 
				{
                    while (true) 
					{
                        int k = fis.read();
                        String t = Integer.toString(k, 2);
                        while ((t.length() < 8)) 
						{
                            t = "0" + t;
                        }
                        str = str + t;
                        for (int i = 0; i <= 8; i++) 
						{
                            int n = st(l, str);
                            if ((!(n == -1)) && (i5 > 0)) 
							{
                                file.write((byte) (n));
                                str = str.substring(l[n].length());
                                i5 = i5 - 1;
                            }
                        }
                        if (i5 % 100000 == 0) 
						{
                            System.out.println(i6 - i5 + "/" + i6);
                        }
                        if (i5 == 0) 
						{
                            file.close();
                            break;
                        }
                    }
                } catch (ArrayIndexOutOfBoundsException e) 
				{
                    file.close();
                }
            } catch (FileNotFoundException e) 
			{
                main(new String[0]);
                System.out.println("No file, try again");
            }
        } catch (FileNotFoundException e) 
		{
            main(new String[0]);
            System.out.println("No file, try again");
        }
    }
}