public class Abc 
{
    public static class ABC extends Thread
	{

        char whoAmI;
        static char whoIsNext = 'A';
        final Object synchro;

        ABC(char whoami, Object sync)
		{
            whoAmI = whoami;
            synchro = sync;
        }

        void MainFunc()
		{
            synchronized(synchro)
			{
                for(int i = 0; i < 5; i++)
				{
                    if(whoIsNext == whoAmI)
					{
                        System.out.print(whoAmI);
                        if(whoAmI == 'A')
						{
                            whoIsNext = 'B';
                        } else if(whoAmI == 'B')
						{
                            whoIsNext = 'C';
                        } else 
						{
                            whoIsNext = 'A';
                        }
                        synchro.notifyAll();
                        try
						{
                            synchro.wait();
                        } catch (InterruptedException e)
						{
                            e.printStackTrace();
                        }
                    } else 
					{
                        i--;
                        synchro.notifyAll();
                        try
						{
                            synchro.wait();
                        } catch (InterruptedException e)
						{
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        @Override
        public void run()
		{
            MainFunc();
        }


    }

    public static void main(String[] args) 
	{

        int s = 10;

        ABC a = new ABC('A', s);
        ABC b = new ABC('B', s);
        ABC c = new ABC('C', s);

        a.start();
        b.start();
        c.start();

    }

}