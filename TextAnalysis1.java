import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.Scanner;

public class Text1
{
    public static String str;

    public static int[] main(String args[]) throws Exception 
	{
        Scanner in = new Scanner(System.in);
        System.out.println("Input your file name");
        str = in.nextLine();
        try 
		{
            FileInputStream fin = new FileInputStream(str);
            int[] m = new int[258];
            try 
			{
                while (true) 
				{
                    m[fin.read()]++;
                }
            } catch (ArrayIndexOutOfBoundsException e) {
                fin.close();
            }
            FileWriter file = new FileWriter(new File("Answer.txt"));
            for (int i = 0; i <= 255; i++) 
			{
                if (!(m[i] == 0)) 
				{
                    file.write(m[i] + " elements with code: " + i + " \"" + (char) i + "\"" + (char) 10);
                }
            }
            file.close();
            return m;
        } catch (FileNotFoundException e) {
            System.out.println("No file, try again");
            return main(new String[0]);
        }
    }
}