import java.io.*;
import java.util.Arrays;


public class Huffman 
{
    public static String[] l;

    public static void stat(Tree t, String s) 
	{
        if (t.left == null) 
		{
            l[(int) t.id] = s;
        } else {
            stat(t.left, s + "0");
            stat(t.right, s + "1");
        }
    }

    public static Tree tree(int[] m, Tree[] tr) 
	{
        while (m[256] > 0) 
		{
            int min = 257;
            int mmin = 257;
            for (int i = 0; i <= 255; i++) 
			{
                if ((m[i] <= m[mmin]) && (!(m[i] == 0))) 
				{
                    if (m[i] <= m[min]) 
					{
                        mmin = min;
                        min = i;
                    } else 
					{
                        mmin = i;
                    }
                }
            }
            tr[min] = new Tree(tr[min], tr[mmin]);
            m[min] = m[min] + m[mmin];
            m[mmin] = 0;
            m[256]--;
        }
        for (int i = 0; i <= 255; i++) 
		{
            if (!(m[i] == 0)) 
			{
                return tr[i];
            }
        }
        return tr[0];
    }

    public static void main(String args[]) throws Exception 
	{
        int[] m = Task031.main(new String[0]);
        l = new String[300];
        FileWriter file = new FileWriter(new File("Answer.txt"), true);
        m[256] = -1;
        m[257] = 0;
        for (int i = 0; i <= 255; i++) 
		{
            if (!(m[i] == 0)) 
			{
                m[256]++;
                m[257] = m[i] + m[257];
            }
        }
        Tree[] tr = new Tree[258];
        for (int i = 0; i <= 255; i++) 
		{
            tr[i] = new Tree((char) i);
        }
        Tree t = tree(m, tr);
        Arrays.fill(l, "");
        stat(t, "");
        for (int i = 0; i <= 255; i++) 
		{
            if (!(l[i].equals(""))) 
			{
                file.write(((char) i) + " - " + l[i] + ((char) 10));
            }
        }
        file.close();
        File f1 = new File("Key");
        int i5 = 0;
        for (int i = 0; i <= 255; i++) 
		{
            i5 = i5 + m[i];
        }
        FileWriter file1 = new FileWriter(f1);
        for (int i = 0; i <= 255; i++) 
		{
            file1.write(l[i] + "\n");
        }
        file1.write(i5 + " ");
        file1.close();
        int k;
        FileInputStream fin = new FileInputStream(Task031.str);
        BufferedOutputStream out = new BufferedOutputStream(new FileOutputStream("Encrypted"));
        String str = "";
        try 
		{
            while (true) 
			{
                k = fin.read();
                str = str + l[k];
                int l = (str.length()) & (~7);
                for (int i = 0; i < l; i = i + 8) 
				{
                    out.write(Integer.parseInt(str.substring(i, i + 8), 2));
                }
                str = str.substring(l);
            }
        } catch (ArrayIndexOutOfBoundsException e) 
		{
            while ((str.length() > 0) && (str.length() < 8))
			{
                str = str + "0";
            }
            out.write(Integer.parseInt(str.substring(0, 8), 2));
        }
        out.close();
    }
}